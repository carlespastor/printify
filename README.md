# Quotify_print / Printify

### Que es Quotify print / Printify?

Quotify print / Printify es un sistema gestor de impresió online que administra els diferents usuaris i cues d'impresio d'una organitzacio o empresa.

### Que pretén proporcionar aquesta tecnologia?

Quotify print / Printify té dos funcionalitats:
	1. Fer una gestió d'usuaris que permet definir les copies permeses per a cada usuari i mantenir un historial d'impresió.
	2. Imprimir un document amb les maximes facilitats i rapidesa. 
	
El gestor permet decidir amb quin dispòsitiu vols fer la impressió informan-te en tot moment quins estan ocupats o lliures, de manera que puguis obtenir la teva impressió el mes rápid i còmode possible 

### Funcionalitats extra

	- Temps d'impressió
	- Previsualización de documento
	- Validacion de documentos
	
## Tecnologies emprades al projecte

	* Symfony (gestió de fitxers i algunews funcionalitats)
	* html/css/js(ajax)/php
	*...
	
## Disseny

Es tracta d'un "one-page" mitjançant ajax on l'usuari mitjançant difetents elements amb els que ineractuar tals com botons, drag-n-drop, pestanyes... pot canviar tot o part del contingut que es mostra sense canviar de direccio ni recarregues de pagina (AJAX).

[Moqup](https://app.moqups.com/a16vicpodmoy@iam.cat/IbqoaHOdet/view/page/a3fc5f8df)