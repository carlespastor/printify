<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Incidencies
 *
 * @ORM\Table(name="INCIDENCIES")
 * @ORM\Entity
 */
class Incidencies
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="Usuari", type="string", length=50, nullable=true)
     */
    private $usuari;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Missatge", type="string", length=255, nullable=true)
     */
    private $missatge;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dataihora", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataihora = 'CURRENT_TIMESTAMP';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set usuari.
     *
     * @param string|null $usuari
     *
     * @return Incidencies
     */
    public function setUsuari($usuari = null)
    {
        $this->usuari = $usuari;

        return $this;
    }

    /**
     * Get usuari.
     *
     * @return string|null
     */
    public function getUsuari()
    {
        return $this->usuari;
    }

    /**
     * Set missatge.
     *
     * @param string|null $missatge
     *
     * @return Incidencies
     */
    public function setMissatge($missatge = null)
    {
        $this->missatge = $missatge;

        return $this;
    }

    /**
     * Get missatge.
     *
     * @return string|null
     */
    public function getMissatge()
    {
        return $this->missatge;
    }

    /**
     * Set dataihora.
     *
     * @param \DateTime $dataihora
     *
     * @return Incidencies
     */
    public function setDataihora($dataihora)
    {
        $this->dataihora = $dataihora;

        return $this;
    }

    /**
     * Get dataihora.
     *
     * @return \DateTime
     */
    public function getDataihora()
    {
        return $this->dataihora;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct() {
        $this->dataihora = new \DateTime();
        $this->dataihora->format('m/d/Y');

    }
}
