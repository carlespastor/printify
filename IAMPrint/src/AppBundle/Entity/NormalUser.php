<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NormalUser
 *
 * @ORM\Table(name="normal_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NormalUserRepository")
 */
class NormalUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(name="id_departament", type="integer", nullable=true)
     */
    private $idDepartament;

    /**
     * @var bool
     *
     * @ORM\Column(name="admin", type="boolean")
     */
    private $admin;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return NormalUser
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return NormalUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set idDepartament
     *
     * @param integer $idDepartament
     *
     * @return NormalUser
     */
    public function setIdDepartament($idDepartament)
    {
        $this->idDepartament = $idDepartament;

        return $this;
    }

    /**
     * Get idDepartament
     *
     * @return int
     */
    public function getIdDepartament()
    {
        return $this->idDepartament;
    }

    /**
     * Set admin
     *
     * @param boolean $admin
     *
     * @return NormalUser
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin
     *
     * @return bool
     */
    public function getAdmin()
    {
        return $this->admin;
    }
}

