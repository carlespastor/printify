<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Impresoras
 *
 * @ORM\Table(name="IMPRESORAS")
 * @ORM\Entity
 */
class Impresoras
{
   
    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=20, nullable=true)
     */
    private $nom;

  
    /**
     * @var bool|null
     *
     * @ORM\Column(name="color", type="boolean", nullable=true)
     */
    private $color;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set ip.
     *
     * @param int|null $ip
     *
     * @return Impresoras
     */
    public function setIp($ip = null)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return int|null
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set nom.
     *
     * @param string|null $nom
     *
     * @return Impresoras
     */
    public function setNom($nom = null)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string|null
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set idDepartament.
     *
     * @param int|null $idDepartament
     *
     * @return Impresoras
     */
    public function setIdDepartament($idDepartament = null)
    {
        $this->idDepartament = $idDepartament;

        return $this;
    }

    /**
     * Get idDepartament.
     *
     * @return int|null
     */
    public function getIdDepartament()
    {
        return $this->idDepartament;
    }

    /**
     * Set color.
     *
     * @param bool|null $color
     *
     * @return Impresoras
     */
    public function setColor($color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color.
     *
     * @return bool|null
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
