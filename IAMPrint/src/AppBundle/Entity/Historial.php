<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historial
 *
 * @ORM\Table(name="HISTORIAL")
 * @ORM\Entity
 */
class Historial
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="id_user", type="integer", nullable=true)
     */
    private $idUser;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_impresora", type="integer", nullable=true)
     */
    private $idImpresora;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom_fitxer", type="string", length=15, nullable=true)
     */
    private $nomFitxer;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ubicació_fitxer", type="string", length=500, nullable=true)
     */
    private $ubicacióFitxer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dataihora", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataihora = 'CURRENT_TIMESTAMP';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set idUser.
     *
     * @param int|null $idUser
     *
     * @return Historial
     */
    public function setIdUser($idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser.
     *
     * @return int|null
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idImpresora.
     *
     * @param int|null $idImpresora
     *
     * @return Historial
     */
    public function setIdImpresora($idImpresora = null)
    {
        $this->idImpresora = $idImpresora;

        return $this;
    }

    /**
     * Get idImpresora.
     *
     * @return int|null
     */
    public function getIdImpresora()
    {
        return $this->idImpresora;
    }

    /**
     * Set nomFitxer.
     *
     * @param string|null $nomFitxer
     *
     * @return Historial
     */
    public function setNomFitxer($nomFitxer = null)
    {
        $this->nomFitxer = $nomFitxer;

        return $this;
    }

    /**
     * Get nomFitxer.
     *
     * @return string|null
     */
    public function getNomFitxer()
    {
        return $this->nomFitxer;
    }

    /**
     * Set ubicacióFitxer.
     *
     * @param string|null $ubicacióFitxer
     *
     * @return Historial
     */
    public function setUbicacióFitxer($ubicacióFitxer = null)
    {
        $this->ubicacióFitxer = $ubicacióFitxer;

        return $this;
    }

    /**
     * Get ubicacióFitxer.
     *
     * @return string|null
     */
    public function getUbicacióFitxer()
    {
        return $this->ubicacióFitxer;
    }

    /**
     * Set dataihora.
     *
     * @param \DateTime $dataihora
     *
     * @return Historial
     */
    public function setDataihora($dataihora)
    {
        $this->dataihora = $dataihora;

        return $this;
    }

    /**
     * Get dataihora.
     *
     * @return \DateTime
     */
    public function getDataihora()
    {
        return $this->dataihora;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
