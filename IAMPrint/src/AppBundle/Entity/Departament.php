<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Departament
 *
 * @ORM\Table(name="DEPARTAMENT")
 * @ORM\Entity
 */
class Departament
{
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=20, nullable=true)
     */
    private $nom;

    /**
     * @var integer
     *
     * @ORM\Column(name="planta", type="integer", nullable=true)
     */
    private $planta;

    /**
     * @var integer
     *
     * @ORM\Column(name="copies_color", type="integer", nullable=true)
     */
    private $copiesColor;

    /**
     * @var integer
     *
     * @ORM\Column(name="copies_blancinegre", type="integer", nullable=true)
     */
    private $copiesBlancinegre;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    public function setId($id) {
        $this->id = $id;
    
        return $this;
    }
    
    public function getId() {
        return $this->id;
    }


    public function setNom($nom) {
        $this->nom = $nom;
    
        return $this;
    }
    
    public function getNom() {
        return $this->nom;
    }


    public function setCopiesColor($copiesColor) {
        $this->copiesColor = $copiesColor;
    
        return $this;
    }
    
    public function getCopiesColor() {
        return $this->copiesColor;
    }

    public function setCopiesBlancinegre($copiesBlancinegre) {
        $this->copiesBlancinegre = $copiesBlancinegre;
    
        return $this;
    }
    
    public function getCopiesBlancinegre() {
        return $this->copiesBlancinegre;
    }

    public function getPlanta() {
        return $this->planta;
    }

    public function setPlanta($planta) {
        $this->planta = $planta;
    
        return $this;
    }

}

