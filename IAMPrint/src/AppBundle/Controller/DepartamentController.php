<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Departament;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Departament controller.
 *
 * @Route("adminpage/departament")
 */
class DepartamentController extends Controller
{
    /**
     * Lists all departament entities.
     *
     * @Route("/", name="adminpage_departament_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $departaments = $em->getRepository('AppBundle:Departament')->findAll();

        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        return $this->render('departament/index.html.twig', array(
            'departaments' => $departaments,
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Creates a new departament entity.
     *
     * @Route("/new", name="adminpage_departament_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $departament = new Departament();
        $form = $this->createForm('AppBundle\Form\DepartamentType', $departament);
        $form->handleRequest($request);

        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($departament);
            $em->flush();

            return $this->redirectToRoute('adminpage_departament_show', array('id' => $departament->getId()));
        }

        return $this->render('departament/new.html.twig', array(
            'departament' => $departament,
            'form' => $form->createView(),
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Finds and displays a departament entity.
     *
     * @Route("/{id}", name="adminpage_departament_show")
     * @Method("GET")
     */
    public function showAction(Departament $departament)
    {
        $deleteForm = $this->createDeleteForm($departament);

        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        return $this->render('departament/show.html.twig', array(
            'departament' => $departament,
            'delete_form' => $deleteForm->createView(),
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Displays a form to edit an existing departament entity.
     *
     * @Route("/{id}/edit", name="adminpage_departament_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Departament $departament)
    {
        $deleteForm = $this->createDeleteForm($departament);
        $editForm = $this->createForm('AppBundle\Form\DepartamentType', $departament);
        $editForm->handleRequest($request);

        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('adminpage_departament_edit', array('id' => $departament->getId()));
        }

        return $this->render('departament/edit.html.twig', array(
            'departament' => $departament,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Deletes a departament entity.
     *
     * @Route("/{id}", name="adminpage_departament_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Departament $departament)
    {
        $form = $this->createDeleteForm($departament);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($departament);
            $em->flush();
        }

        return $this->redirectToRoute('adminpage_departament_index');
    }

    /**
     * Creates a form to delete a departament entity.
     *
     * @param Departament $departament The departament entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Departament $departament)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('adminpage_departament_delete', array('id' => $departament->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
