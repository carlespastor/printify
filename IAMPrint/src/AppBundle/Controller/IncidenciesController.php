<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Incidencies;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Incidency controller.
 *
 * @Route("adminpage/incidencies")
 */
class IncidenciesController extends Controller
{
    /**
     * Lists all incidency entities.
     *
     * @Route("/", name="adminpage_incidencies_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $incidencies = $em->getRepository('AppBundle:Incidencies')->findAll();

        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        return $this->render('incidencies/index.html.twig', array(
            'incidencies' => $incidencies,
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Creates a new incidency entity.
     *
     * @Route("/new", name="adminpage_incidencies_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $incidency = new Incidencies();
        $form = $this->createForm('AppBundle\Form\IncidenciesType', $incidency);
        $form->handleRequest($request);

        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($incidency);
            $em->flush();

            return $this->redirectToRoute('adminpage_incidencies_show', array('id' => $incidency->getId()));
        }

        return $this->render('incidencies/new.html.twig', array(
            'incidency' => $incidency,
            'form' => $form->createView(),
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Finds and displays a incidency entity.
     *
     * @Route("/{id}", name="adminpage_incidencies_show")
     * @Method("GET")
     */
    public function showAction(Incidencies $incidency)
    {
        $deleteForm = $this->createDeleteForm($incidency);
        
        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        return $this->render('incidencies/show.html.twig', array(
            'incidency' => $incidency,
            'delete_form' => $deleteForm->createView(),
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Displays a form to edit an existing incidency entity.
     *
     * @Route("/{id}/edit", name="adminpage_incidencies_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Incidencies $incidency)
    {
        $deleteForm = $this->createDeleteForm($incidency);
        $editForm = $this->createForm('AppBundle\Form\IncidenciesType', $incidency);
        $editForm->handleRequest($request);

        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('adminpage_incidencies_edit', array('id' => $incidency->getId()));
        }

        return $this->render('incidencies/edit.html.twig', array(
            'incidency' => $incidency,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Deletes a incidency entity.
     *
     * @Route("/{id}", name="adminpage_incidencies_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Incidencies $incidency)
    {
        $form = $this->createDeleteForm($incidency);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($incidency);
            $em->flush();
        }

        return $this->redirectToRoute('adminpage_incidencies_index');
    }

    /**
     * Creates a form to delete a incidency entity.
     *
     * @param Incidencies $incidency The incidency entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Incidencies $incidency)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('adminpage_incidencies_delete', array('id' => $incidency->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
