<?php

namespace AppBundle\Controller;

use AppBundle\Entity\NormalUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Normaluser controller.
 *
 * @Route("adminpage/normaluser")
 */
class NormalUserController extends Controller
{
    /**
     * Lists all normalUser entities.
     *
     * @Route("/", name="adminpage_normaluser_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        $normalUsers = $em->getRepository('AppBundle:NormalUser')->findAll();

        return $this->render('normaluser/index.html.twig', array(
            'normalUsers' => $normalUsers,
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Creates a new normalUser entity.
     *
     * @Route("/new", name="adminpage_normaluser_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $normalUser = new Normaluser();
        $form = $this->createForm('AppBundle\Form\NormalUserType', $normalUser);
        $form->handleRequest($request);

        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($normalUser);
            $em->flush();

            return $this->redirectToRoute('adminpage_normaluser_show', array('id' => $normalUser->getId()));
        }

        return $this->render('normaluser/new.html.twig', array(
            'normalUser' => $normalUser,
            'form' => $form->createView(),
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Finds and displays a normalUser entity.
     *
     * @Route("/{id}", name="adminpage_normaluser_show")
     * @Method("GET")
     */
    public function showAction(NormalUser $normalUser)
    {
        $deleteForm = $this->createDeleteForm($normalUser);
        
        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        return $this->render('normaluser/show.html.twig', array(
            'normalUser' => $normalUser,
            'delete_form' => $deleteForm->createView(),
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Displays a form to edit an existing normalUser entity.
     *
     * @Route("/{id}/edit", name="adminpage_normaluser_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, NormalUser $normalUser)
    {
        $deleteForm = $this->createDeleteForm($normalUser);
        $editForm = $this->createForm('AppBundle\Form\NormalUserType', $normalUser);
        $editForm->handleRequest($request);

        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('adminpage_normaluser_edit', array('id' => $normalUser->getId()));
        }

        return $this->render('normaluser/edit.html.twig', array(
            'normalUser' => $normalUser,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Deletes a normalUser entity.
     *
     * @Route("/{id}", name="adminpage_normaluser_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, NormalUser $normalUser)
    {
        $form = $this->createDeleteForm($normalUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($normalUser);
            $em->flush();
        }

        return $this->redirectToRoute('adminpage_normaluser_index');
    }

    /**
     * Creates a form to delete a normalUser entity.
     *
     * @param NormalUser $normalUser The normalUser entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(NormalUser $normalUser)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('adminpage_normaluser_delete', array('id' => $normalUser->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
