<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\NormalUser;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class AdminController extends Controller
{
   /**
     * @Route("/adminpage", name="adminpage")
     */
    public function adminpageAction(Request $request)
    {
            //Sentencia per mostrar tots els departaments a la part admin
            $repository = $this->getDoctrine()->getRepository('AppBundle:Departament');
            $departament = $repository->findAll();

            //Per mostrar l'historial General amb el seu Usuari
            $stmt = $this->getDoctrine()->getEntityManager()
            ->getConnection()
            ->prepare('select * from HISTORIAL INNER JOIN fos_user ON HISTORIAL.id_user = fos_user.id;');
            $stmt->execute();
            $historic = $stmt->fetchAll();


            //Mostrar totes les incidencies amb el seu husuari
            $stmt = $this->getDoctrine()->getEntityManager()
            ->getConnection()
            ->prepare('select * from INCIDENCIES');
            $stmt->execute();
            $incidencies = $stmt->fetchAll();

            //$usuaris = new NormalUser();
            $usuaris = array("Usuaris" => "Aqui van els usuaris","NombreUsuaris" => "Aqui va el nombre de usuaris", "Departament" => "Departament per als usuaris");

            $form = $this->createFormBuilder($usuaris)
            	->add('usuaris', TextareaType::class)
            	->add('nombreUsuaris', NumberType::class)
            	->add('departament', TextType::class)
            	->add('save', SubmitType::class, array('label' => ' '))
            	->getForm();

            $form->handleRequest($request);

            $entityManager = $this->getDoctrine()->getManager();

            if ($form->isSubmitted() && $form->isValid()) {
	            
	            $users = $form->getData();
	            $delimiter = ",";




	            $usuaris = explode ( $delimiter , $users["usuaris"] );



	            for ($i=0; $i < $users["nombreUsuaris"]; $i++) { 

	            	$delimiter = "@";
	            	$nombre = explode ( $delimiter , $usuaris[$i] );
	            	$idDepartament = $repository->findOneBy(['nom' => $users["departament"]])->getId();

	            	$normalUser = new NormalUser();
	            	$normalUser->setNom($nombre[0]);
	                $normalUser->setEmail($usuaris[$i]);
	                $normalUser->setIdDepartament($idDepartament);
	                $normalUser->setAdmin(0);
	                $entityManager->persist($normalUser);
	                $entityManager->flush();
	            }

                return $this->redirect($request->getUri());
	            

            }
            

            //Entitat per a treure el llistat de totes les impressores
            $impresor = $this->getDoctrine()->getRepository('AppBundle:Impresoras')->findAll();

            //Agafa el nom de l'usuari
            $usr = $this->get('security.token_storage')->getToken()->getUser();
            $usuari = $usr->getUsername();
            $email = $usr->getEmail();
    	
            return $this->render('admin/adminpage.html.twig',array(
                'nomusuari' => $usuari,
                'email'=> $email,
                'departament' => $departament,
                'historic' => $historic,
                'incidencies' => $incidencies,
                'impresor' => $impresor,
                'form' => $form->createView(),

            ));


    }

    
}