<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\NormalUser;
use AppBundle\Entity\Incidencies;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/acces_denegat", name="denegat")
     */
    public function denegatAction(Request $request)
    {
        $entityManager2 = $this->getDoctrine()->getRepository("AppBundle:NormalUser");
        $email_admin=$entityManager2->findOneBy(['admin' => 1])->getEmail();
        // replace this example code with whatever you need
        return $this->render('default/denegat.html.twig' ,array(
            'mailAdmin' => $email_admin
        ));
    }


    /**
     * @Route("/error", name="error")
     */
    public function errorAction(Request $request)
    {

        return $this->render('default/error.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);  
                 
    }


    /**
     * @Route("/impressio", name="impressio")
     */
    public function printAction(Request $request)
    {   

          //Insertar usuario
          //----------------------------------------------------------------------------------------


          $entityManager2 = $this->getDoctrine()->getRepository("AppBundle:NormalUser");
          $entityManager3 = $this->getDoctrine()->getManager();
          $entityManager4 = $this->getDoctrine()->getRepository("AppBundle:User");


          

          $troba_tot= $entityManager2->findAll();
          $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
          $email=$usuari->getEmail();
          $email_usuari=$usuari->getEmail();
          $nom_usuari=$usuari->getUserName();
          $troba_per_nom= $entityManager2->findOneBy(['nom' => $nom_usuari]);
          $troba_per_email= $entityManager2->findOneBy(['email' => $email]);
          $idFos= $usuari->getId();

          //echo $idFos;


          $normalUser = new NormalUser();
          
          if ($troba_tot==null) {
            if($troba_per_email==false){
                $normalUser->setNom($usuari);
                $normalUser->setEmail($email);
                $normalUser->setAdmin(1);
                $entityManager3->persist($normalUser);
                $entityManager3->flush();
            }
            
                
          }
          else{
            if($troba_per_email==false){

                $nom_usuari_fos=$entityManager4->findOneBy(['username' => $nom_usuari])->getUsername();
                $sql = $this->container->get('database_connection');
                $consulta = $sql->query("SELECT * FROM fos_user WHERE username='$nom_usuari_fos'");
                $email_admin=$entityManager2->findOneBy(['admin' => 1])->getEmail();

                if($consulta==true){
                    $sql->query("DELETE FROM fos_user WHERE username='$nom_usuari_fos'");
                }
    
                return $this->render('default/no.html.twig',  array(
                    'mailAdmin' => $email_admin
                )); 
            }
            $admin= $entityManager2->findOneBy(['email' => $email])->getAdmin();
            if($troba_per_email==true && $admin==0){
                $buscador=$entityManager3->getRepository('AppBundle:NormalUser')->findOneBy(['email' => $email]);
                $buscador->setNom($usuari);
                $buscador->setEmail($email);
                $buscador->setAdmin(0);
                $entityManager3->persist($buscador);
                $entityManager3->flush();
            }
          }

          $users= $entityManager2->findAll();
          $userAll= $entityManager4->findAll();


          /* Print actions */

          $entityManager = $this->getDoctrine()->getRepository("AppBundle:Impresoras");

          //$entityManager = $this->getDoctrine()->getManager();
          $impresorascolor;
          $idImpColor;
          $impresorasblancoynegro;
          $idImpBlancNegre;
          $i = 0;
          $j = 0;
          $q = $entityManager->findAll();

          foreach ($q as $row) {
            if ($row->getColor()==true) {
              $impresorascolor[$i] = $row->getNom();
              $idImpColor[$i] = $row->getId();
              $i++;
            }
            else{
              $impresorasblancoynegro[$j] = $row->getNom();
              $idImpBlancNegre[$j] = $row->getId();
              $j++;
            }
          }

          $nom = $this->container->get('security.token_storage')->getToken()->getUser();
          $mailcomp = $nom->getEmail();

          $departament = 2;
          $copiesColor = 0;
          $copiesBlancNegre = 0;

          $entityManager = $this->getDoctrine()->getRepository("AppBundle:NormalUser");
          $q = $entityManager->findAll();
          foreach ($q as $row) {
            $mail = $row->getEmail();
            if ($mail == $mailcomp){
              $departament = $row->getIdDepartament();
              $idNormalUser = $row->getId();
            }
          }

          $entityManager = $this->getDoctrine()->getRepository("AppBundle:Departament");
          $q = $entityManager->findAll();
          foreach ($q as $row) {
            if ($row->getId()==$departament) {
              $copiesColor = $row->getCopiesColor();
              $copiesBlancNegre = $row->getCopiesBlancinegre();
            }
          }


        // replace this example code with whatever you need
        return $this->render('default/userprint.html.twig', array(
            'texto' => "Penja els fitxers",
            'nom' => $nom,
            'texto' => "Vista Previa",
            'color' => $impresorascolor,
            'idColor' => $idImpColor,
            'blancinegre' => $impresorasblancoynegro,
            'idBlancNegre' => $idImpBlancNegre,
            'limitcolor' => $i,
            'limitblancinegre' => $j,
            'copiesColor' => $copiesColor,
            'copiesBlancNegre' => $copiesBlancNegre,
            'users' => $users,
            'usuari' => $nom_usuari,
            'email_usuari' => $email_usuari,
            'departament' => $departament,
            'idUser' => $idFos//$idNormalUser
        ));
    }

    /**
     * @Route("/fileuploadhandler", name="fileuploadhandler")
     */
    public function fileUploadHandler(Request $request) {
        $output = array('uploaded' => false);
        // get the file from the request object
        $file = $request->files->get('file');
        // generate a new filename (safer, better approach)
        // To use original filename, $fileName = $this->file->getClientOriginalName();
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $realFileName = $file->getClientOriginalName();

        // set your uploads directory
        $uploadDir = $this->get('kernel')->getRootDir() . '/../web/uploads/';
        if (!file_exists($uploadDir) && !is_dir($uploadDir)) {
            mkdir($uploadDir, 0775, true);
        }
        if ($file->move($uploadDir, $fileName)) {
            $output['uploaded'] = true;
            $output['fileName'] = $fileName;
            $output['realFileName'] = $realFileName;
        }
        return new JsonResponse($output);

    }

    /**
     * @Route("/userpage", name="userpage")
     */
    public function userpageAction(Request $request)
    {
            //Sentencia per mostrar tots els departaments a la part admin
            $repository = $this->getDoctrine()->getRepository('AppBundle:Departament');
            $departament = $repository->findAll();

            //Per mostrar l'historial General amb el seu Usuari
            $stmt = $this->getDoctrine()->getEntityManager()
            ->getConnection()
            ->prepare('select * from HISTORIAL INNER JOIN fos_user ON HISTORIAL.id_user = fos_user.id;');
            $stmt->execute();
            $historic = $stmt->fetchAll();



            //Agafa el nom de l'usuari
            $usr = $this->get('security.token_storage')->getToken()->getUser();
            $usuari = $usr->getUsername();
            $email = $usr->getEmail();

            //Mostrar totes les incidencies amb el seu usuari
            $stmt = $this->getDoctrine()->getEntityManager()
            ->getConnection()
            ->prepare("select * from INCIDENCIES where Usuari='$usuari'");
            $stmt->execute();
            $incidencies = $stmt->fetchAll();

            //Formulario incidencies
            //Se crea el formulario con los datos que se quieren enviar
            $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
            $nom=$usuari->getUserName();

            $form = $this->createFormBuilder()
                ->add('Missatge', TextareaType::class)
                ->getForm();

            $form->handleRequest($request);
            $info_form = $form->getData();
            $Missatge=$form->get('Missatge')->getData();
            $entityManager = $this->getDoctrine()->getManager();

            //Si el formulario se envia y es valido se crea el objeto y 
            //establecemos los valores y por ultimo lo enviamos a la base de datos
            if ($form->isSubmitted() && $form->isValid()) {
                
                $incidencia = new Incidencies();
                $incidencia->setUsuari($nom);
                $incidencia->setMissatge($Missatge);
                $entityManager->persist($incidencia);
                $entityManager->flush();

                return $this->redirect($request->getUri());
                
            }

            return $this->render('default/userpage.html.twig',array(
                'nomusuari' => $usuari,
                'email' => $email,
                'departament' => $departament,
                'historic' => $historic,
                'incidencies' => $incidencies,
                //Aqui se le pasa el formulario a la vista
                'form' => $form->createView(),
            ));



    }

    
}

