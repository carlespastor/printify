<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Impresoras;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Impresora controller.
 *
 * @Route("adminpage/impresoras")
 */
class ImpresorasController extends Controller
{
    /**
     * Lists all impresora entities.
     *
     * @Route("/", name="adminpage_impresoras_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $impresoras = $em->getRepository('AppBundle:Impresoras')->findAll();

        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        return $this->render('impresoras/index.html.twig', array(
            'impresoras' => $impresoras,
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Creates a new impresora entity.
     *
     * @Route("/new", name="adminpage_impresoras_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $impresora = new Impresoras();
        $form = $this->createForm('AppBundle\Form\ImpresorasType', $impresora);
        $form->handleRequest($request);

        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($impresora);
            $em->flush();

            return $this->redirectToRoute('adminpage_impresoras_show', array('id' => $impresora->getId()));
        }

        return $this->render('impresoras/new.html.twig', array(
            'impresora' => $impresora,
            'form' => $form->createView(),
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Finds and displays a impresora entity.
     *
     * @Route("/{id}", name="adminpage_impresoras_show")
     * @Method("GET")
     */
    public function showAction(Impresoras $impresora)
    {
        $deleteForm = $this->createDeleteForm($impresora);
        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        return $this->render('impresoras/show.html.twig', array(
            'impresora' => $impresora,
            'delete_form' => $deleteForm->createView(),
            'nomusuari' => $nom_usuari,
            'email' => $email
            
        ));
    }

    /**
     * Displays a form to edit an existing impresora entity.
     *
     * @Route("/{id}/edit", name="adminpage_impresoras_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Impresoras $impresora)
    {
        $deleteForm = $this->createDeleteForm($impresora);
        $editForm = $this->createForm('AppBundle\Form\ImpresorasType', $impresora);
        $editForm->handleRequest($request);

        $usuari = $this->container->get('security.token_storage')->getToken()->getUser();
        $email=$usuari->getEmail();
        $nom_usuari=$usuari->getUserName();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('adminpage_impresoras_edit', array('id' => $impresora->getId()));
        }

        return $this->render('impresoras/edit.html.twig', array(
            'impresora' => $impresora,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'nomusuari' => $nom_usuari,
            'email' => $email
        ));
    }

    /**
     * Deletes a impresora entity.
     *
     * @Route("/{id}", name="adminpage_impresoras_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Impresoras $impresora)
    {
        $form = $this->createDeleteForm($impresora);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($impresora);
            $em->flush();
        }

        return $this->redirectToRoute('adminpage_impresoras_index');
    }

    /**
     * Creates a form to delete a impresora entity.
     *
     * @param Impresoras $impresora The impresora entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Impresoras $impresora)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('adminpage_impresoras_delete', array('id' => $impresora->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
