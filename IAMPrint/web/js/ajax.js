function ajax(nombrEncriptado, nombreReal){
    //console.log(nombre);

    $(".less").click(function(){
        $num = $("#copies").val();
        if($num>0){
            $num--;
            $("#copies").val($num);
        }
    });

    $(".plus").click(function(){
        $num = $("#copies").val();
        if($num<10000){
            $num++;
            $("#copies").val($num);
        }
    });

    $('#color, #cares, #orientacio').prop('checked', false);

    var comp;

    $("#orientacio, #oriText").hide();

    $('#cares').change(function() {
        if($(this).is(":checked")) {
            $("#orientacio, #oriText").show();
        }
        else{
            $("#orientacio, #oriText").hide();
        }       
    });

    $(".selectimpresora .negre li, .selectimpresora .color li").click(function(){

        $("#fase3").addClass("missatgeimpressio");
        $("#redo").addClass("clickableback");
        
        var impresoraEscollida = $(this).children("div").text();
        var copies = $("#copies").val();
        var cares = "una sola cara";
        var orientacio = "vertical";
        if($("#orientacio").prop('checked')){
            orientacio = "horitzontal";
        }
        if($("#cares").prop('checked')){
            cares = "doble cara";
        }
        var color = "Blanc i negre"
        if($("#color").prop('checked')){
            color = "Color";
        }
        $("#insertinfo").html("<h2>Informació:</h2><br><p>Impresora: " + impresoraEscollida + 
                         "</p><p>Numero de copies: " + copies +
                         "</p><p>Tipus d'impressio: " + color + ", " + cares + " i " + orientacio +
                         "</p><br><br><a class='button' href='http://localhost:8001/impressio'>Imprimir un altre fitxer</a>");

        $("#redocross").removeClass("invisible")
        $("#redocross").addClass("exitcross")

        $("#fase2").css("opacity", ".1");
        $("#vistaDocumento").css("opacity", ".1");

        comp = true;

        var dept = $("#userinfo").data("dept");
        
        var idUser = $("#userinfo").data("iduser");
        var idImp = $(this).children("div").data("id");

        console.log(idImp);


        console.log("ESTE: " + idUser + " " + idImp + " " + nombreReal + " " + nombrEncriptado);


        txt(impresoraEscollida, copies, color, cares, nombreReal, nombrEncriptado, orientacio);
        restacopies(copies, dept, color);
        historic(idUser, idImp, nombreReal, nombrEncriptado);

    });


    $("#redo, #redocross").on("click", function(){
        if (comp) {
            $("#fase3").removeClass("missatgeimpressio");
            $("#redocross").addClass("invisible")
            $("#redocross").removeClass("exitcross")
            $("#redo").removeClass("clickableback");
            $("#insertinfo").html("");
            $("#fase2").css("opacity", "1");
        }
    });

    $(document).keyup(function(event){
        if(event.which==27){
            if (comp) {
                $("#fase3").removeClass("missatgeimpressio");
                $("#redocross").addClass("invisible")
                $("#redocross").removeClass("exitcross")
                $("#redo").removeClass("clickableback");
                $("#insertinfo").html("");
                $("#fase2").css("opacity", "1");
            }
        }
    });


    parametros={
        nombre_archivo : nombrEncriptado,
        carpeta : "uploads" //si se cambiara el nombre de la carpeta, solo hay que cambiar el nombre aqui
    };
    

    $("#fase1").hide();
    $("#fase2, #vistaDocumento").show();

    verpdf(parametros);

    $(".color").hide();

    $("#color").change(function(){

        if($("#color").prop('checked')){
            $(".negre").hide();
            $(".color").show();
        }
        else{
            $(".color").hide();
            $(".negre").show();
        }
    });

    $("#back").click(function(){
        $("#fase2").hide();
        $("#fase1").show();
    })

}