function txt(impresoraEscollida, copies, color, cares, nombreReal, nombrEncriptado, orientacio){

    if (orientacio=="vertical"){
        gir ="two-sided-long-edge";
    }
    else{
        gir ="two-sided-short-edge";
    }

    parametros={
        impresoraEscollida : impresoraEscollida,
        copies : copies,
        color : color,
        cares : cares,
        nombre : nombrEncriptado,
        nombreReal : nombreReal,
        gir : gir,
    };
    
    $.ajax({
        method:'POST',
        url: 'php/txt.php',
        data: parametros,
        dataType : 'text',
        success: function (datos){
            console.log("Output:" + datos);
        },
        error: function (datos){
            alert("Error a l'impressio, contacta amb l'administrador.");
        }
    })

}

function restacopies(copies, dept, tipus){
    parametros={
        copies : copies,
        dept : dept,
        tipus : tipus
    };
    $.ajax({
        type:'POST',
        data: parametros,
        url: 'php/restacopies.php',
        dataType : 'text',
        success: function (datos){
            console.log(datos);
        },
        error: function (datos){
            console.log("Error en reasignacion de copias");
        }
    })
}

function historic(idUser, idImp, nombreReal, nombrEncriptado){
    parametros={
        idUser : idUser,
        idImp : idImp,
        nombreReal : nombreReal,
        nombrEncriptado : nombrEncriptado
    };
    $.ajax({
        type:'POST',
        data: parametros,
        url: 'php/historic.php',
        dataType : 'text',
        success: function (datos){
            console.log(datos);
        },
        error: function (datos){
            console.log("Error en historic");
        }
    })
}