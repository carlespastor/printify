$(function(){

        //When page loads...
        $("#form_save").hide();
        $("#form_nombreUsuaris").parent("div").hide();
        $("#form_departament").parent("div").hide();
        $("#form_usuaris").parent("div").hide();
        $(".tab-content").hide(); //Hide all content
        $("ul.tabs li:first").addClass("active").show(); //Activate first tab
        $(".tab-content:first").show(); //Show first tab content

        $("ul.tabs li").click(function () {

            $("ul.tabs li").removeClass("active"); //Remove any "active" class
            $(this).addClass("active"); //Add "active" class to selected tab
            $(".tab-content").hide(); //Hide all tab content

            var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
            $(activeTab).fadeIn(); //Fade in the active ID content
            return false;
        });

        $("#submit").click(function(){
            $("#form_save").trigger("click");
        });

        $("#depart option").click(function(){
            $dept = $(this).val();
            $("#form_departament").val($dept);
        });

        var totalResults = 0;
        var arrayUser = [];
        $("#form_nombreUsuaris").val(0);

        $("#submitEmails").on("click",function(){
            
            var emails = $("#insercioIndividual").val();
            
            
            var separador = " ";
            var resultados = emails.split(separador);

            var num_emails = resultados.length;

            for (var i=0; i < resultados.length; i++) {
                var results = resultados[i].split(",");
                for (var j=0; j < results.length; j++) {
                    if (results[j]!="") {

                        var comp=0;
                        var li;

                        $("#emailsañadidos ul li").each(function(){
                            if ($(this).text()==results[j]) {
                                comp=1;
                            }
                            
                        });

                        if (comp==0){
                            function validateEmail(email) {
                              var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                              return re.test(email);
                            }
                            if (validateEmail(results[j])) {
                                $("#emailsañadidos ul").append("<li>" + results[j] + "</li>");
                                arrayUser.push(results[j]);
                                totalResults++;
                                $("#error").html("<p></p>");
                            }
                            else {
                                $("#error").html("<p>Email no valid (" + results[j] + ")</p>");
                            }
                        }
                        else{
                            //$("#emailsañadidos span").text("Hay emails repetidos.");
                            $("#error").html("<p>Emails Repetidos</p>");
                        }
                    }
                }
            }

            $("#form_nombreUsuaris").val(totalResults);
            $("#form_usuaris").val(arrayUser);

            

        });

        
        
});